#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/gpio/consumer.h>
#include <linux/clk.h>
#include <linux/delay.h>
#include <linux/io.h>
#include <linux/dma-mapping.h>
#include <linux/slab.h>
#include <drm/drm_drv.h>
#include <drm/drm_mode_config.h>
#include <drm/drm_atomic_helper.h>
#include <drm/drm_fb_helper.h>
#include <drm/drm_gem_cma_helper.h>
#include <drm/drm_gem_framebuffer_helper.h>
#include <drm/drm_simple_kms_helper.h>
#include <drm/drm_probe_helper.h>
#include <linux/regulator/consumer.h>

#include "elcdif_regs.h"

#define ILI9481_CMD_NOP 			0x00
#define ILI9481_CMD_SOFTRST 		0x01
#define ILI9481_CMD_EXIT_SLEEP_MODE 0x11
#define ILI9481_CMD_SET_DISPLAY_OFF 0x28
#define ILI9481_CMD_SET_DISPLAY_ON  0x29
#define ILI9481_CMD_SET_COLUMN_ADDR 0x2a
#define ILI9481_CMD_SET_PAGE_ADDR	0x2b
#define ILI9481_CMD_WRITE_MEM_START 0x2c
#define ILI9481_CMD_SET_ADDR_MODE 	0x36
#define ILI9481_CMD_SET_PIXEL_FMT 	0x3a
#define ILI9481_CMD_MEM_ACCESS_AND_IF_SETTING 0xb3
#define ILI9481_CMD_DISPLAY_MODE_AND_FMEM_MODE 0xb4
#define ILI9481_CMD_DEVICE_CODE_READ 	0xbf
#define ILI9481_CMD_PANEL_DRIVE_SETTING 0xc0
#define ILI9481_CMD_FRAME_RATE 		0xc5
#define ILI9481_CMD_INTERFACE_CTRL 	0xc6
#define ILI9481_CMD_GAMMA 			0xc8
#define ILI9481_CMD_POWER 			0xd0
#define ILI9481_CMD_VCOM 			0xd1
#define ILI9481_CMD_POWER_SETTING_NORMAL 0xd2

enum {
	MPU_DATA,
	MPU_CMD,
};

enum {
	MPU_READ,
	MPU_WRITE,
};

// ----------------------------------------------
struct lcdif_mpu_private {
	struct device *dev; // Device
	void __iomem *base; // peripheral base address
	struct clk *pixClk; // Pixel clock
	struct clk *axiClk; // Axi clock
	struct gpio_desc *ioRst; // Reset pin
	struct backlight_device *backlight; // Backlight
	int rotation; // Orientation of the panel

	struct drm_device drm;
	struct drm_simple_display_pipe pipe;
	struct drm_connector conn;
	struct drm_display_mode mode;
};

static const struct of_device_id lcdif_mpu_dt_ids[] = {
        {.compatible = "fsl,imx6ul-lcdif-mpu"},
        {.compatible = "robojan,imx6ul-lcdif-mpu"},
		{}
};
MODULE_DEVICE_TABLE(of, lcdif_mpu_dt_ids);

static struct lcdif_mpu_private *drm_to_lcdif_priv(struct drm_device *drm)
{
	return container_of(drm, struct lcdif_mpu_private, drm);
}

static void lcdif_mpu_reset(struct lcdif_mpu_private *priv) 
{
	// Enable clock gate and soft reset
	writel(CTRL_SFTRST, priv->base + LCDC_CTRL + REG_SET);
	writel(CTRL_CLKGATE, priv->base + LCDC_CTRL + REG_SET);
	// Disable clock gate followed by soft reset
	writel(CTRL_CLKGATE, priv->base + LCDC_CTRL + REG_CLR);
	writel(CTRL_SFTRST, priv->base + LCDC_CTRL + REG_CLR);
	// Asume the the instructions following this write are slow 
	// enough to ensure the reset is completed
}

static int lcdif_mpu_lcd_reset(struct lcdif_mpu_private *priv) 
{
	if(priv->ioRst) {
		gpiod_set_value_cansleep(priv->ioRst, 0);
		usleep_range(20, 1000);
		gpiod_set_value_cansleep(priv->ioRst, 1);
		msleep(100);
		return 0;
	} else {
		return -ENOSYS;
	}
}

static void lcdif_mpu_enter_command_mode(struct lcdif_mpu_private *priv)
{
	// Input 32 bit ARGB, output 16 bit RGB565, no shifting, or swapping
	writel(CTRL_INPUT_SWIZZLE(SWIZZLE_LE) | CTRL_SHIFT_DIR(0) | CTRL_SHIFT_NUM(0) |
		CTRL_WORD_LENGTH_16 | CTRL_BUS_WIDTH_16, 
		priv->base + LCDC_CTRL);
	writel(CTRL1_SET_BYTE_PACKAGING(0x3), priv->base + LCDC_CTRL1);
	writel(CTRL2_OUTSTANDING_REQS(REQ_2) | CTRL2_READ_MODE_NUM_PACKED_SUBWORDS(1) |
	 		CTRL2_INITIAL_DUMMY_READ(1), priv->base + LCDC_V4_CTRL2);	
}

static void lcdif_mpu_enter_data_mode(struct lcdif_mpu_private *priv)
{
	// Input 32 bit ARGB, output 16 bit RGB565, no shifting, or swapping
	writel(CTRL_MASTER | CTRL_INPUT_SWIZZLE(SWIZZLE_LE) | CTRL_SHIFT_DIR(0) | CTRL_SHIFT_NUM(0) |
		CTRL_WORD_LENGTH_24 | CTRL_BUS_WIDTH_16, 
		priv->base + LCDC_CTRL);
	writel(CTRL1_SET_BYTE_PACKAGING(0x7), priv->base + LCDC_CTRL1);
	
}

static int lcdif_mpu_wait_for_ready(struct lcdif_mpu_private *priv)
{
	unsigned int val;
	int timeout = 0;

	// Check for running
	val = readl(priv->base + LCDC_CTRL);
	while(val & CTRL_RUN) {
		mdelay(1);
		timeout ++;
		if (timeout >= 1000) {
			dev_err(priv->dev, "lcdif_mpu_wait_for_ready timeout!\n");
			return -ETIME;
		}
		val = readl(priv->base + LCDC_CTRL);
	}

	return 0;
}

unsigned int lcdif_mpu_access(struct lcdif_mpu_private *priv, int mode, int rw, int data)
{
	unsigned int val, wordlen, ret = 0;

	if (lcdif_mpu_wait_for_ready(priv) != 0)
		return 0;

	writel(CTRL_MASTER,
			 priv->base + LCDC_CTRL + REG_CLR);

	writel(CTRL1_BYTE_PACKING_FORMAT_MASK,
			 priv->base + LCDC_CTRL1 + REG_CLR);
	val = readl(priv->base + LCDC_CTRL);
	wordlen = CTRL_GET_WORD_LENGTH(val);
	writel(CTRL_WORD_LENGTH_MASK,
			 priv->base + LCDC_CTRL + REG_CLR);

	writel(CTRL_YCBCR422_INPUT | 
			 CTRL_INPUT_SWIZZLE_MASK,
			 priv->base + LCDC_CTRL + REG_CLR);

	writel(CTRL1_SET_BYTE_PACKAGING(3),
			priv->base + LCDC_CTRL1 + REG_SET);
	writel(CTRL_WORD_LENGTH_16,
			priv->base + LCDC_CTRL + REG_SET);

	val = TRANSFER_COUNT_SET_VCOUNT(1) | TRANSFER_COUNT_SET_HCOUNT(1);
	writel(val, priv->base + LCDC_V4_TRANSFER_COUNT);

	if(mode == MPU_CMD)
	{
		writel(CTRL_DATA_SELECT,
				 priv->base + LCDC_CTRL + REG_CLR);
	}
	else
	{
		writel(CTRL_DATA_SELECT,
				 priv->base + LCDC_CTRL + REG_SET);
	}

	if(rw == MPU_READ)
	{
		writel(CTRL2_READ_MODE_NUM_PACKED_SUBWORDS_MASK,
				priv->base + LCDC_V4_CTRL2 + REG_CLR);
		writel(CTRL2_READ_MODE_NUM_PACKED_SUBWORDS(1),
				 priv->base + LCDC_V4_CTRL2 + REG_SET);

		writel(CTRL_READ_WRITEB,
				 priv->base + LCDC_CTRL + REG_SET);
		writel(CTRL_RUN,
				 priv->base + LCDC_CTRL + REG_SET);
	}
	else
	{
		writel(CTRL_READ_WRITEB,
				 priv->base + LCDC_CTRL + REG_CLR);
		writel(CTRL_RUN,
				 priv->base + LCDC_CTRL + REG_SET);

		writel(data, priv->base + LCDC_V4_DATA);
	}

	val = readl(priv->base + LCDC_CTRL);
	while(val & CTRL_RUN)
	{
		if(rw == MPU_READ)
			ret = readl(priv->base + LCDC_V4_DATA);

		val = readl(priv->base + LCDC_CTRL);
	}

	writel(CTRL_MASTER,
			 priv->base + LCDC_CTRL + REG_SET);

	writel(CTRL_WORD_LENGTH_MASK,
			 priv->base + LCDC_CTRL + REG_CLR);
	writel(CTRL_SET_WORD_LENGTH(wordlen),
			 priv->base + LCDC_CTRL + REG_SET);  // 32 bits valid data
	writel(CTRL1_BYTE_PACKING_FORMAT_MASK,
			 priv->base + LCDC_CTRL1 + REG_CLR);
	writel(CTRL1_SET_BYTE_PACKAGING(0xF),
			 priv->base + LCDC_CTRL1 + REG_SET);  // 32 bits valid data

	writel(CTRL_MASTER,
			 priv->base + LCDC_CTRL + REG_SET);

	return ret;
}


unsigned int lcdif_mpu_access_write_data(struct lcdif_mpu_private *priv, const u8 *data, int size)
{
	unsigned int val, wordlen, i, ret = 0;
	unsigned int timeout = 0;

	if (lcdif_mpu_wait_for_ready(priv) != 0)
		return 0;

	writel(CTRL_MASTER,
			 priv->base + LCDC_CTRL + REG_CLR);

	writel(CTRL1_BYTE_PACKING_FORMAT_MASK,
			 priv->base + LCDC_CTRL1 + REG_CLR);
	val = readl(priv->base + LCDC_CTRL);
	wordlen = CTRL_GET_WORD_LENGTH(val);
	writel(CTRL_WORD_LENGTH_MASK,
			 priv->base + LCDC_CTRL + REG_CLR);

	writel(CTRL_YCBCR422_INPUT | 
			 CTRL_INPUT_SWIZZLE_MASK,
			 priv->base + LCDC_CTRL + REG_CLR);

	writel(CTRL1_SET_BYTE_PACKAGING(3),
			priv->base + LCDC_CTRL1 + REG_SET);
	writel(CTRL_WORD_LENGTH_16,
			priv->base + LCDC_CTRL + REG_SET);

	val = TRANSFER_COUNT_SET_VCOUNT(size) | TRANSFER_COUNT_SET_HCOUNT(1);
	writel(val, priv->base + LCDC_V4_TRANSFER_COUNT);

	writel(CTRL_DATA_SELECT,
				priv->base + LCDC_CTRL + REG_SET);

	writel(CTRL_READ_WRITEB,
				priv->base + LCDC_CTRL + REG_CLR);

	writel(CTRL_RUN,
				priv->base + LCDC_CTRL + REG_SET);
	for(i = 0; i < size; i++) {

		writel(data[i], priv->base + LCDC_V4_DATA);
	}
	val = readl(priv->base + LCDC_CTRL);
	while(val & CTRL_RUN)
	{
		timeout ++;
		if (timeout >= 1000) {
			dev_err(priv->dev, "lcdif_mpu_access_write_data timeout!\n");
			return -ETIME;
		}
		val = readl(priv->base + LCDC_CTRL);
	}

	writel(CTRL_MASTER,
			 priv->base + LCDC_CTRL + REG_SET);

	writel(CTRL_WORD_LENGTH_MASK,
			 priv->base + LCDC_CTRL + REG_CLR);
	writel(CTRL_SET_WORD_LENGTH(wordlen),
			 priv->base + LCDC_CTRL + REG_SET);  // 32 bits valid data
	writel(CTRL1_BYTE_PACKING_FORMAT_MASK,
			 priv->base + LCDC_CTRL1 + REG_CLR);
	writel(CTRL1_SET_BYTE_PACKAGING(0xF),
			 priv->base + LCDC_CTRL1 + REG_SET);  // 32 bits valid data

	writel(CTRL_MASTER,
			 priv->base + LCDC_CTRL + REG_SET);

	return ret;
}

static void lcdif_mpu_send_write_cmd(struct lcdif_mpu_private *priv, u8 cmd, const void *buf, int size)
{
	lcdif_mpu_access(priv, MPU_CMD, MPU_WRITE, cmd);
	if(size > 0) {
		lcdif_mpu_access_write_data(priv, buf, size);
	}
}

static int lcdif_mpu_setColumnAddr(struct lcdif_mpu_private *priv, int start, int end)
{
	u8 buf[4];
	buf [0] = (start >> 8) & 0xFF;
	buf [1] = (start >> 0) & 0xFF;
	buf [2] = (end >> 8) & 0xFF;
	buf [3] = (end >> 0) & 0xFF;

	lcdif_mpu_send_write_cmd(priv, ILI9481_CMD_SET_COLUMN_ADDR, buf, 4);
	return 0;
}

static int lcdif_mpu_setPageAddr(struct lcdif_mpu_private *priv, int start, int end)
{
	u8 buf[4];
	buf [0] = (start >> 8) & 0xFF;
	buf [1] = (start >> 0) & 0xFF;
	buf [2] = (end >> 8) & 0xFF;
	buf [3] = (end >> 0) & 0xFF;

	lcdif_mpu_send_write_cmd(priv, ILI9481_CMD_SET_PAGE_ADDR, buf, 4);
	return 0;	
}

static int lcdif_mpu_setAddress(struct lcdif_mpu_private *priv, int startX, int endX, int startY, int endY)
{
	lcdif_mpu_setColumnAddr(priv, startX, endX);
	lcdif_mpu_setPageAddr(priv, startY, endY);
	return 0;
}

static int lcdif_mpu_lcd_init(struct lcdif_mpu_private *priv)
{
	u8 buf[12];

	lcdif_mpu_lcd_reset(priv);

	lcdif_mpu_enter_command_mode(priv);

	// Init sequence
	lcdif_mpu_send_write_cmd(priv, ILI9481_CMD_SOFTRST, NULL, 0);
	msleep(10);
	lcdif_mpu_send_write_cmd(priv, ILI9481_CMD_EXIT_SLEEP_MODE, NULL, 0);
	msleep(50);
	buf[0] = 0x08;
	buf[1] = 0x44;
	buf[2] = 0x1e;
	lcdif_mpu_send_write_cmd(priv, ILI9481_CMD_POWER, buf, 3);
	buf[0] = 0x00;
	buf[1] = 0x0c;
	buf[2] = 0x1a;
	lcdif_mpu_send_write_cmd(priv, ILI9481_CMD_VCOM, buf, 3);
	buf[0] = 0x03;
	lcdif_mpu_send_write_cmd(priv, ILI9481_CMD_FRAME_RATE, buf, 1);
	buf[0] = 0x01;
	buf[1] = 0x11;
	lcdif_mpu_send_write_cmd(priv, ILI9481_CMD_POWER_SETTING_NORMAL, buf, 2);
	buf[0] = 0xa0;
	lcdif_mpu_send_write_cmd(priv, 0xe4, buf, 1);
	buf[0] = 0x00;
	buf[1] = 0x2a;
	lcdif_mpu_send_write_cmd(priv, 0xf3, buf, 2);
	buf[0] = 0x00;
	buf[1] = 0x26;
	buf[2] = 0x21;
	buf[3] = 0x00;
	buf[4] = 0x00;
	buf[5] = 0x1f;
	buf[6] = 0x65;
	buf[7] = 0x23;
	buf[8] = 0x77;
	buf[9] = 0x00;
	buf[10] = 0x0f;
	buf[11] = 0x00;
	lcdif_mpu_send_write_cmd(priv, ILI9481_CMD_GAMMA, buf, 12);
	buf[0] = 0x00;
	buf[1] = 0x3b;
	buf[2] = 0x00;
	buf[3] = 0x02;
	buf[4] = 0x11;
	buf[5] = 0x01;
	lcdif_mpu_send_write_cmd(priv, ILI9481_CMD_PANEL_DRIVE_SETTING, buf, 6);
	buf[0] = 0x83;
	lcdif_mpu_send_write_cmd(priv, ILI9481_CMD_INTERFACE_CTRL, buf, 1);
	buf[0] = 0x01;
	lcdif_mpu_send_write_cmd(priv, 0xf0, buf, 1);
	buf[0] = 0x0a;
	lcdif_mpu_send_write_cmd(priv, 0xe4, buf, 1);
	buf[0] = 0x88;
	lcdif_mpu_send_write_cmd(priv, ILI9481_CMD_SET_ADDR_MODE, buf, 1);
	buf[0] = 0x55;
	lcdif_mpu_send_write_cmd(priv, ILI9481_CMD_SET_PIXEL_FMT, buf, 1);
	buf[0] = 0x02;
	buf[1] = 0x00;
	buf[2] = 0x00;
	buf[3] = 0x01;
	lcdif_mpu_send_write_cmd(priv, ILI9481_CMD_MEM_ACCESS_AND_IF_SETTING, buf, 4);

	// lcdif_mpu_setAddress(priv, 0, priv->lcdWidth, 0, priv->lcdHeight);

	dev_info(priv->dev, "LCD initialized");

	return 0;
}

static int lcdif_mpu_init(struct lcdif_mpu_private *priv)
{
	int err;

	// Enable the clocks
	err = clk_prepare_enable(priv->pixClk);
	if(err < 0)
	{
		dev_err(priv->dev, "Failed to enable pixel clock: %d", err);
		return err;
	}
	err = clk_prepare_enable(priv->axiClk);
	if(err < 0)
	{
		dev_err(priv->dev, "Failed to enable bus clock: %d", err);
		return err;
	}

	dev_dbg(priv->dev, "clocks: %lu Hz %lu Hz", clk_get_rate(priv->pixClk), clk_get_rate(priv->axiClk));

	// Reset the eLCDIF
	lcdif_mpu_reset(priv);

	// Setup the timing
	writel(TIMING_CMD_HOLD(2) | TIMING_CMD_SETUP(2) | 
		   TIMING_DATA_SETUP(2) | TIMING_DATA_HOLD(1), 
		   priv->base + LCDC_TIMING);

	// return test_if(priv);

	lcdif_mpu_enter_command_mode(priv);

	return lcdif_mpu_lcd_init(priv);
}

static int lcdif_mpu_get_modes(struct drm_connector *conn)
{
	struct lcdif_mpu_private *priv = container_of(conn, struct lcdif_mpu_private, conn);
	struct drm_display_mode *mode;
	
	mode = drm_mode_duplicate(conn->dev, &priv->mode);
	if(!mode) {
		dev_err(priv->dev, "Failed to duplicate mode\n");
		return 0;
	}

	drm_mode_set_name(mode);
	mode->type |= DRM_MODE_TYPE_PREFERRED;
	drm_mode_probed_add(conn, mode);
	conn->display_info.width_mm = mode->width_mm;
	conn->display_info.height_mm = mode->height_mm;
	switch(priv->rotation) {
	case 90:
		conn->display_info.panel_orientation = DRM_MODE_PANEL_ORIENTATION_RIGHT_UP;
		break;
	case 180:
		conn->display_info.panel_orientation = DRM_MODE_PANEL_ORIENTATION_BOTTOM_UP;
		break;
	case 270:
		conn->display_info.panel_orientation = DRM_MODE_PANEL_ORIENTATION_LEFT_UP;
		break;
	default:
		conn->display_info.panel_orientation = DRM_MODE_PANEL_ORIENTATION_NORMAL;
		break;
	}

	dev_info(priv->dev, "get modes %d", 1);

	return 1;
}

static const struct drm_connector_helper_funcs lcdif_mpu_conn_helper_funcs = {
	.get_modes = lcdif_mpu_get_modes,
};

static const struct drm_connector_funcs lcdif_mpu_conn_funcs = {
	.fill_modes = drm_helper_probe_single_connector_modes,
	.destroy = drm_connector_cleanup,
	.reset = drm_atomic_helper_connector_reset,
	.atomic_duplicate_state = drm_atomic_helper_connector_duplicate_state,
	.atomic_destroy_state = drm_atomic_helper_connector_destroy_state,
};

static int lcdif_mpu_conn_init(struct lcdif_mpu_private *priv)
{
	drm_connector_helper_add(&priv->conn, &lcdif_mpu_conn_helper_funcs);
	return drm_connector_init(&priv->drm, &priv->conn, 
		&lcdif_mpu_conn_funcs, DRM_MODE_CONNECTOR_DPI);
}

static enum drm_mode_status lcdif_mpu_mode_valid(
	struct drm_simple_display_pipe *pipe,
	const struct drm_display_mode *mode)
{
	struct drm_device *drm = pipe->crtc.dev;
	struct lcdif_mpu_private *priv = drm_to_lcdif_priv(drm);
	if(mode->hdisplay != priv->mode.hdisplay || mode->vdisplay != priv->mode.vdisplay) {
		return MODE_BAD;
	}
	return MODE_OK;
}

static void lcdif_mpu_enable(struct drm_simple_display_pipe *pipe, 
							 struct drm_crtc_state *crtc_state,
							 struct drm_plane_state *plane_state)
{
	struct drm_device *drm = pipe->crtc.dev;
	struct lcdif_mpu_private *priv = drm_to_lcdif_priv(drm);
	int idx;
	dev_info(priv->dev, "Enabling screen");

	if(!drm_dev_enter(drm, &idx))
		return;

	lcdif_mpu_enter_command_mode(priv);

	lcdif_mpu_send_write_cmd(priv, ILI9481_CMD_SET_DISPLAY_ON, NULL, 0);
	if(priv->backlight) {
		backlight_enable(priv->backlight);
	} 

	drm_dev_exit(idx);
}

static void lcdif_mpu_disable(struct drm_simple_display_pipe *pipe)
{
	struct drm_device *drm = pipe->crtc.dev;
	struct lcdif_mpu_private *priv = drm_to_lcdif_priv(drm);
	dev_info(priv->dev, "Disabling screen");

	lcdif_mpu_enter_command_mode(priv);

	if(priv->backlight) {
		backlight_disable(priv->backlight);
	}
	lcdif_mpu_send_write_cmd(priv, ILI9481_CMD_SET_DISPLAY_OFF, NULL, 0);
}

static void lcdif_mpu_update(struct drm_simple_display_pipe *pipe, struct drm_plane_state *old_state)
{
	struct drm_plane_state *state = pipe->plane.state;
	struct drm_device *drm = pipe->crtc.dev;
	struct lcdif_mpu_private *priv = drm_to_lcdif_priv(drm);
	struct drm_framebuffer *fb = state->fb;
	struct drm_gem_object *gem;
	struct drm_gem_cma_object *cma_obj;
	dma_addr_t tr;
	int idx;
	uint32_t val;
	int width, height;

	if(!pipe->crtc.state->active)
		return;

	if(!drm_dev_enter(fb->dev, &idx))
		return;

	gem = drm_gem_fb_get_obj(fb, 0);
	cma_obj = to_drm_gem_cma_obj(gem);

	width = priv->mode.hdisplay;
	height = priv->mode.vdisplay;

	lcdif_mpu_setAddress(priv, 0, width, 0, height);
	
	lcdif_mpu_access(priv, MPU_CMD, MPU_WRITE, ILI9481_CMD_WRITE_MEM_START);
	
	tr = cma_obj->paddr;

	lcdif_mpu_enter_data_mode(priv);

	if (lcdif_mpu_wait_for_ready(priv) != 0)
		return;

	val = TRANSFER_COUNT_SET_HCOUNT(width) | TRANSFER_COUNT_SET_VCOUNT(height);
	writel(val, priv->base + LCDC_V4_TRANSFER_COUNT);

	writel(CTRL_READ_WRITEB,
			 priv->base + LCDC_CTRL + REG_CLR);
	writel(CTRL_BYPASS_COUNT,
			 priv->base + LCDC_CTRL + REG_CLR);
	writel(CTRL_DATA_SELECT,
			 priv->base + LCDC_CTRL + REG_SET);

	writel(tr, priv->base + LCDC_V4_NEXT_BUF);
	writel(tr, priv->base + LCDC_V4_CUR_BUF);

	writel(CTRL_MASTER,
			 priv->base + LCDC_CTRL + REG_SET);
	writel(CTRL_RUN,
			 priv->base + LCDC_CTRL + REG_SET);

}

static const struct drm_simple_display_pipe_funcs lcdif_mpu_pipe_funcs ={
	.mode_valid = lcdif_mpu_mode_valid,
	.enable = lcdif_mpu_enable,
	.disable = lcdif_mpu_disable,
	.update = lcdif_mpu_update,
};

static const uint32_t lcdif_mpu_formats[] = {
	DRM_FORMAT_ARGB8888,
	DRM_FORMAT_XRGB8888,
};

static const uint64_t lcdif_mpu_modifiers[] = {
	DRM_FORMAT_MOD_LINEAR,
	DRM_FORMAT_MOD_INVALID,
};

static int lcdif_mpu_pipe_init(struct lcdif_mpu_private *priv)
{
	return drm_simple_display_pipe_init(
		&priv->drm,
		&priv->pipe,
		&lcdif_mpu_pipe_funcs,
		lcdif_mpu_formats,
		ARRAY_SIZE(lcdif_mpu_formats),
		lcdif_mpu_modifiers,
		&priv->conn
	);
}

static const struct drm_mode_config_funcs lcdif_mpu_mode_config_funcs = {
	.fb_create = drm_gem_fb_create_with_dirty,
	.atomic_check = drm_atomic_helper_check,
	.atomic_commit = drm_atomic_helper_commit,
};

static int lcdif_mpu_modeset_init(struct lcdif_mpu_private *priv)
{
	int ret;
	struct drm_mode_config *mode_config;
	struct drm_device *drm = &priv->drm;

	ret = drmm_mode_config_init(drm);
	if(ret) {
		return ret;
	}

	mode_config = &drm->mode_config;
	mode_config->funcs = &lcdif_mpu_mode_config_funcs;
	mode_config->min_width = priv->mode.hdisplay;
	mode_config->max_width = priv->mode.hdisplay;
	mode_config->min_height = priv->mode.vdisplay;
	mode_config->max_height = priv->mode.vdisplay;

	return 0;
}

DEFINE_DRM_GEM_CMA_FOPS(drm_fops);

static struct drm_driver lcdif_drm_driver = 
{
	.driver_features = DRIVER_MODESET | DRIVER_GEM | DRIVER_ATOMIC,
	.name = "lcdif_mpu",
	.desc = "DRM module for iMX6 eLCDIF with ili9481",
	.date = "20210725",
	.major = 1,
	.minor = 0,
	.patchlevel = 0,
	.fops = &drm_fops,
	DRM_GEM_CMA_DRIVER_OPS_VMAP,
};

static int lcdif_mpu_probe(struct platform_device *pdev)
{
	int ret;
	int err;
	// unsigned int bufferSize;
	struct resource *res;
	struct lcdif_mpu_private *priv;
	struct device *dev = &pdev->dev;
	struct drm_device *drm;
	u32 pixClkRate=0;

	// If we don't have a device tree node return.
	if (!pdev->dev.of_node)
		return -ENODEV;

	// Allocate driver structure
	priv = devm_drm_dev_alloc(dev, &lcdif_drm_driver, struct lcdif_mpu_private, drm);
	if(IS_ERR(priv))
		return PTR_ERR(priv);

	// Get the pointers
	drm = &priv->drm;
	
	// Get the eLCDIF peripheral base register
	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	priv->base = devm_ioremap_resource(dev, res);
	if(IS_ERR(priv->base)) {
		return PTR_ERR(priv->base);	
	}

	// Get the clocks
	priv->pixClk = devm_clk_get(dev, "pix");
	if(IS_ERR(priv->pixClk)) {
		err = PTR_ERR(priv->pixClk);
		return err;
	}
	if(of_property_read_u32(pdev->dev.of_node, "pixclk-rate-hz", &pixClkRate) == 0) {
		// Set the rate if specified
		dev_dbg(dev, "Setting pixel clock rate to %u Hz", pixClkRate);
		err = clk_set_rate(priv->pixClk, pixClkRate);
		if(err < 0) {
			dev_warn(dev, "Error setting pixel clock rate to %u Hz: %d", pixClkRate, err);
		}
	}

	priv->axiClk = devm_clk_get(dev, "axi");
	if(IS_ERR(priv->axiClk)) {
		err = PTR_ERR(priv->axiClk);
		return err;
	}

	// Get the GPIOs
	priv->ioRst = devm_gpiod_get(dev, "reset", GPIOD_OUT_LOW);
	if(IS_ERR(priv->ioRst)) {
		priv->ioRst = NULL;
	}

	// Get the backlight
	priv->backlight = devm_of_find_backlight(dev);
	if(IS_ERR(priv->backlight)) {
		priv->backlight = NULL;
		dev_warn(dev, "Cannot find backlight");
	}

	// Get properties
	{
		int lcdWidth, lcdHeight, lcdWidthMm, lcdHeightMm;
		if(of_property_read_s32(pdev->dev.of_node, "lcd-width", &lcdWidth) != 0) {
			dev_err(dev, "Missing property lcd-width");
			return -EINVAL;
		}
		if(of_property_read_s32(pdev->dev.of_node, "lcd-height", &lcdHeight) != 0) {
			dev_err(dev, "Missing property lcd-height");
			return -EINVAL;
		}
		if(of_property_read_s32(pdev->dev.of_node, "lcd-width-mm", &lcdWidthMm) != 0) {
			dev_err(dev, "Missing property lcd-width-mm");
			return -EINVAL;
		}
		if(of_property_read_s32(pdev->dev.of_node, "lcd-height-mm", &lcdHeightMm) != 0) {
			dev_err(dev, "Missing property lcd-height-mm");
			return -EINVAL;
		}
		if(of_property_read_s32(pdev->dev.of_node, "rotation", &priv->rotation) != 0) {
			priv->rotation = 0;
		}
		{
			struct drm_display_mode mode = {
				DRM_SIMPLE_MODE(lcdWidth, lcdHeight, lcdWidthMm, lcdHeightMm),
			};
			drm_mode_copy(&priv->mode, &mode);
		}
	}

	// Save the driver data
	priv->dev = dev;

	// Initialize the LCD controller
	dev_info(dev, "Initializing LCDIF MPU controller");
	ret = lcdif_mpu_init(priv);
	if(ret)
		return ret;

	// Setup DRM
	dev_info(dev, "Initializing modeset");
	ret = lcdif_mpu_modeset_init(priv);
	if(ret) {
		return ret;
	}

	dev_info(dev, "Initializing connector");
	ret = lcdif_mpu_conn_init(priv);
	if(ret) {
		return ret;
	}

	dev_info(dev, "Initializing pipe");
	ret = lcdif_mpu_pipe_init(priv);
	if(ret) {
		return ret;
	}

	dev_info(dev, "Resetting mode config");
	drm_mode_config_reset(drm);


	platform_set_drvdata(pdev, priv);

	dev_info(dev, "Registering DRM device");
	ret = drm_dev_register(drm, 0);
	if(ret) {
		return ret;
	}

	dev_info(dev, "Setting up FBDEV");
	drm_fbdev_generic_setup(drm, 0);

	dev_info(dev, "Loaded LCDIF MPU driver");

	return ret;
}

static int lcdif_mpu_remove(struct platform_device *pdev)
{
	struct lcdif_mpu_private *priv = platform_get_drvdata(pdev);
	struct drm_device *drm = &priv->drm;
	dev_info(&pdev->dev, "Removed LCDIF MPU driver");
	// drm_dev_unregister(drm);
	drm_dev_unplug(drm);
	drm_atomic_helper_shutdown(drm);
	dev_info(&pdev->dev, "Removed LCDIF MPU driver");

	return 0;
}

static void lcdif_mpu_shutdown(struct platform_device *pdev)
{
	struct lcdif_mpu_private *priv = platform_get_drvdata(pdev);
	struct drm_device *drm = &priv->drm;
	drm_atomic_helper_shutdown(drm);
}

static struct platform_driver lcdif_mpu_platform_driver = {
        .probe = lcdif_mpu_probe,
        .remove = lcdif_mpu_remove,
		.shutdown = lcdif_mpu_shutdown,
        .driver = {
                .name = "lcdifdrm_mpu",
                .of_match_table = lcdif_mpu_dt_ids,
        }
};

module_platform_driver(lcdif_mpu_platform_driver);

MODULE_AUTHOR("Robbert-Jan de Jager <robojan1@hotmail.com>");
MODULE_DESCRIPTION("DRM driver for Lab power supply");
MODULE_LICENSE("GPL v2");